# requirements: 
# WSL2 (Im using ubuntu20.04) and docker
# 
# Install x11docker wie folgt
( Siehe auch https://github.com/mviereck/x11docker#installation  and  https://github.com/mviereck/x11docker/wiki/x11docker-on-MS-Windows  )
# 1. Install 'X Server':

- Cygwin ( https://cygwin.com/setup-x86_64.exe ) with packages: 'xinit' , 'xauth' via
```
setup-x86.exe --quiet-mode --packages xinit, xauth
```
# 2. Install 'runx': ( https://github.com/mviereck/runx ) in WSL via
```
sudo wget https://raw.githubusercontent.com/mviereck/runx/master/runx -O /usr/local/bin/runx
sudo chmod +x /usr/local/bin/runx
sudo apt update
sudo apt install xauth
```
# 3. Install x11docker via
```
curl -fsSL https://raw.githubusercontent.com/mviereck/x11docker/master/x11docker | sudo bash -s -- --update
```
# 4. Build and run Docker Image [ Zwingend in bash ausführebn, d.h. "cmd.exe" -> "wsl bash" ]
(Docker pyqt5 example basically taken from:  https://github.com/jozo/docker-pyqt5 ) 
build docker image 
```
 docker build -t pyqt_test .
```
# start docker image
```
x11docker pyqt_test
```


### 
Die `runx` in diesem Verzeichnis muss evtl die /usr/local/bin/runx ersetzen, damti der Startbefehl den richtigen Bildschirm ansteuert. 
Siehe Parameter bei https://x.cygwin.com/docs/man1/XWin.1.html ; 
insbesondere die Befehle "-screen 0 @x" mit x in {1,2,...} und -rootless bewirkt fensterlose vollbild darstellung auf Monitor x:

Dies ist hoffentlich zukünftig über Parameter steuerbar (d.h. x11docker übergibt entsprechende Befehle an runx, welche die Befehle wiederum an XWin weitergibt). Ansonsten müssen diese Anwendungen von mir/uns angepasst werden / um diese Funktionalität erweitert werden.


Entsprechender geänderter Block in runx:
```
# Seamless or desktop mode
#[ "$Desktopmode" = "no" ] && Xcommand="$Xcommand -multiwindow" # auskommentiert.
[ "$Desktopmode" = "no" ] && Xcommand="$Xcommand -rootless" #  added line.
    
# Clipboard
case $Shareclipboard in
yes) Xcommand="$Xcommand -clipboard" ;;
no)  Xcommand="$Xcommand -noclipboard" ;;
esac

# Screensize
#[ "$Screensize" ] && Xcommand="$Xcommand -screen 0 '$Screensize'" # auskommentiert
Xcommand="$Xcommand -screen 0 @2" ## added line
```