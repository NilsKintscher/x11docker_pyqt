import sys
from PyQt5.QtWidgets import QApplication, QWidget, QLabel


if __name__ == "__main__":
    app = QApplication(sys.argv)

    screen = app.primaryScreen()
    size = screen.size()
    rect = screen.availableGeometry()

    # Build the window widget
    w = QWidget()
    w.setGeometry(0, 0, 1920, 500)  # x, y, w, h
    w.setWindowTitle("My First Qt App")

    # Add a label with tooltip
    label = QLabel(
        """Hello World
    Screen: %s
    Size: %d x %d
    Available: %d x %d """
        % (screen.name(), size.width(), size.height(), rect.width(), rect.height()),
        w,
    )
    label.setToolTip("This is a <b>QLabel</b> widget with Tooltip")
    label.resize(label.sizeHint())
    label.move(80, 50)

    # Show window and run
    w.show()
    app.exec_()
